# Quality Assurance
**_PDF's AND PIPELINE NOT INCLUDED IN THIS REPO_**

Quality Assurance TEX presentations, homework and example code  
By: Héctor Fierros López

**NOTE**: Reviewed and edited by Marco Cordero

## Current progress (**DONE**)
- [x] SampleClass
- [x] Week_1_Class_1
- [x] Week_2_Class_1
- [x] Week_2_Class_2
- [x] Week_3_Class_1
- [x] Week_3_Class_2
- [x] Week_4_Class_1
- [x] Week_5_Class_2
- [x] Week_6_Class_2
- [x] Week_7_Class_1
- [x] Week_7_Class_2
- [x] Week_8_Class_2
- [x] Week_9_Class_1
- [~] Week_9_Class_2
- [x] Week_10_Class_2
- [x] Week_11_Class_1
- [x] Week_11_Class_2 (First half is a duplicate from previous week)
- [x] Week_12_Class_1 (Only class file)
- [x] Week_14_Class_2 (Only class file)
- [x] Week_15_Class_1
- [x] Week_16_Class_1
- [x] Week_17_Class_1